import sys


def intersection(r1, r2, r3, r4):
    x11, y11 = r1
    x12, y12 = r2

    x21, y21 = r3
    x22, y22 = r4

    x1 = None
    y1 = None

    x2 = None
    y2 = None

    # 1st Rectangle

    if x11 < x21 < x12:
        x1 = x21
    
    if y11 < y21 < y12:
        y1 = y21

    if x11 < x22 < x12:
        x2 = x22

    if y11 < y22 < y12:
        y2 = y22

    # 2nd Rectangle

    if x21 < x11 < x22:
        x1 = x11

    if y21 < y11 < y22:
        y1 = y11

    if x21 < x12 < x22:
        x2 = x12

    if y21 < y12 < y22:
        y2 = y12


    if x1 == None or y1 == None or x2 == None or y2 == None:
        return None, None
    return (x1, y1), (x2, y2)



TopLeftX1 = int(sys.argv[1])
TopLeftY1 = int(sys.argv[2])
BottomRightX1 = int(sys.argv[3])
BottomRightY1 = int(sys.argv[4])

TopLeftX2 = int(sys.argv[5])
TopLeftY2 = int(sys.argv[6])
BottomRightX2 = int(sys.argv[7])
BottomRightY2 = int(sys.argv[8])

X, Y = intersection((TopLeftX1, TopLeftY1), (BottomRightX1, BottomRightY1), (TopLeftX2, TopLeftY2), (BottomRightX2, BottomRightY2))

if X == None or Y == None:
    print(None)
else:
    print(X,',', Y)

