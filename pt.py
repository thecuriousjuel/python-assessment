import sys

r = int(sys.argv[1])

mylist = []
for i in range(r):
    s = ''
    n = 1
    for j in range(1, r-i):
        s += '  '   

    for k in range(i+1):
        s += '   ' + str(n)
        n = int(n * (i - k) / (k + 1))
    s += '\n'

    mylist.append(s)

for i in range(len(mylist)-1,-1, -1):
    print(mylist[i], end = '')